Artist: Saty
Album: Introlence
Label: USC
Catalog number: USC-WR-1212.0136
Release Year: 2012

Composed by Saty.
All instruments programmed, music written and arranged by Sergey Saty.
Cover photos by Johnny Myreng Henriksen and David DeHetre. Artwork by Mike Winchester.

This release is licensed under Creative Commons Attribution-Share Alike.

Official site: http://unitedstudios.ru/
Blog: http://uscu.unitedstudios.ru/
Discogs: http://www.discogs.com/label/USC 
Last.fm: http://www.last.fm/label/USC
Kroogi.com: http://usc.kroogi.com/
Bandcamp: http://bc.unitedstudios.ru/
Internet Archive: http://archive.org/details/usc-label
Vkontakte: http://vk.com/unitedstudios